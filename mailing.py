#!/usr/bin/env python
# -*- coding: utf-8 -*-

from inactives import Inactives

# main

if __name__ == '__main__':

    inactives = Inactives()

    inactives_ids = inactives.id()

    inactives.delete(inactives_ids)

    account_ids, account_emails, account_last_sign_in_ats = inactives.get_inactives()

    account_usernames = inactives.usernames(account_ids)

    inactives.mailing(account_ids, account_emails, account_usernames, account_last_sign_in_ats) 

