# mailing

Mail your Mastodon server inactive users and track their feedback.

This code written in Python get all more than six months  inactive users from your Mastodon's database and email them with the subject and message of your choice.
Then, inactive users data is stored into new created Postgresql database to track feedback and status.
Run mailing.py periodically to catch 'new' inactive users and update the elapsed days of the already emailed ones.

### Dependencies

-   **Python 3**
-   Postgresql server
-   Mastodon server (admin)

### Usage:

Within Python Virtual Environment:

1. Run `python db-setup.py` to set database parameters and create needed database and table. All collected data of inactive users (see point 3) will be written there.

2. Run `python setup.py` to set your SMTP parameters and desired email subject. Also set your Mastodon's full path. They will be saved to `secrets/secrets.txt` for further use.

3. Run `python mailing.py` to start emailing your inactive users (`current_sign_in_at` column older than six months). Their username, account_id, email, delivery status (True if successful) and delivery date will be written to Postgresql database. There is another column, `deleted`, False by default. Will be useful to track deleted/not deleted inactive users if you choose to do so.

4. Use your favourite scheduling method to set mailing.py to run regularly. Column 'elapsed_days' of mailing's database will be updated so you can decide actions after some time.  

5. Run `python delete_inactives.py` to delete all inactive users who replied yes to deletion and all the rest with no feedback after 31 days period from the warning email.

6. Run `python edit_status.py` to set True or False any of following `mailing_db_table` columns: `to_be_deleted`, `feedback` and `recipient_error`. Useful after emailed user's feedback.

Note: install all needed packages with `pip install -r requirements.txt`
