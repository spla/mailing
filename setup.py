#!/usr/bin/env python
# -*- coding: utf-8 -*-

import getpass
import fileinput,re
import os
import sys

def create_dir():
  if not os.path.exists('secrets'):
    os.makedirs('secrets')

def create_file():
  if not os.path.exists('secrets/secrets.txt'):
    with open('secrets/secrets.txt', 'w'): pass
    print(secrets_filepath + " created!")

def write_parameter( parameter, file_path ):
  print("\n")
  print("Setting up SMTP parameters...")
  smtp_host = input("Enter SMTP hostname: ")
  smtp_user_login = input("Enter SMTP user login, ex. user@" + smtp_host +"? ")
  smtp_user_password = getpass.getpass("SMTP user password? ")
  email_subject = input("Enter the subject of the email: ")
  mastodon_full_path = input("Enter Mastodon's live dir full path, ex. /home/mastodon/live : ")
  with open(file_path, "w") as text_file:
    print("smtp_host: {}".format(smtp_host), file=text_file)
    print("smtp_user_login: {}".format(smtp_user_login), file=text_file)
    print("smtp_user_password: {}".format(smtp_user_password), file=text_file)
    print("email_subject: {}".format(email_subject), file=text_file)
    print("mastodon_full_path: {}".format(mastodon_full_path), file=text_file)

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, creating it."%file_path)
        create_dir()
        create_file()
        write_parameter( parameter, file_path )
        print("\n")
        print("SMTP setup done!\n")
        print("Now you can run mailing.py!")

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + " Missing parameter %s "%parameter)
    sys.exit(0)

# Load secrets from secrets file
secrets_filepath = "secrets/secrets.txt"
smtp_host = get_parameter("smtp_host", secrets_filepath)
smtp_user_login = get_parameter("smtp_user_login", secrets_filepath)
smtp_user_pass = get_parameter("smtp_user_pass", secrets_filepath)
email_subject = get_parameter("email_subject", secrets_filepath)
mastodon_full_path = get_parameter("mastodon_full_path", secrets_filepath)

