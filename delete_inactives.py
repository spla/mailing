#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime, timezone, timedelta
import time
import os
import sys
import os.path
import psycopg2

def delete_inactives(deletion_accepted, query):

    id_array = []

    username_array = []

    conn = None

    try:

        conn = psycopg2.connect(database = mailing_db, user = mailing_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        cur.execute(query)

        for row in cur:

            id_array.append(row[0])

            username_array.append(row[1])

        i = 0

        if len(id_array) == 0:

            if deletion_accepted == True:

                print("None inactive users who accepted to be deleted found!")

            elif deletion_accepted == False:

                print("None inactive users to be deleted!")

            return

        while i < len(id_array):

            if deletion_accepted == True:

                print("Deleting inactive users who accepted to be deleted...")

            elif deletion_accepted == False:

                print("Deleting inactive users who do not reply our email...")

            print(f"\nDeleting user {str(i)} of {str(len(id_array))} users")

            print(f"Deleting user with id {str(id_array[i])}, username: {username_array[i]}")

            os.system(f"RAILS_ENV=production {rvm_ruby} {mastodon_full_path}/bin/tootctl accounts delete {username_array[i]}")

            delete_user(id_array[i], username_array[i])

            i += 1

        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

def delete_user(id, username):

    conn = None

    try:

        conn = psycopg2.connect(database = mailing_db, user = mailing_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        cur.execute("DELETE FROM " + mailing_db_table + " where account_id=(%s)", (str(id),))

        print(f"Deleting user {str(id)}, username {str(username)}")

        conn.commit()

        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

def get_parameter( parameter, file_path ):

    # Check if secrets file exists
    if not os.path.isfile(file_path):

        if file_path == "secrets/secrets.txt":

            print("File %s not found, exiting. Run setup.py."%file_path)

        elif file_path == "config.txt":

            print("File %s not found, exiting. Run db-setup.py."%file_path)

        sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:

        for line in f:

            if line.startswith( parameter ):

                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(f"{file_path} Missing parameter {parameter}")
    print("Run setup.py")
    sys.exit(0)

def config():

    secrets_filepath = "secrets/secrets.txt"
    mastodon_full_path = get_parameter("mastodon_full_path", secrets_filepath)

    # Load configuration from config file
    config_filepath = "config.txt"
    mailing_db = get_parameter("mailing_db", config_filepath)
    mailing_db_user = get_parameter("mailing_db_user", config_filepath)
    mailing_db_table = get_parameter("mailing_db_table", config_filepath)

    return (mastodon_full_path, mailing_db, mailing_db_user, mailing_db_table)

###############################################################################
# main

if __name__ == '__main__':

    mastodon_full_path, mailing_db, mailing_db_user, mailing_db_table = config()

    global rvm_ruby
    rvm_ruby = os.environ['HOME'] + "/.rbenv/shims/ruby"

    ###############################################################################
    # select and delete users who replied the warning email saying yes to deletion

    deletion_accepted = True

    query = "select account_id, username, email, to_be_deleted, feedback, recipient_error, elapsed_days from " + mailing_db_table + " where to_be_deleted and feedback"

    delete_inactives(deletion_accepted, query)

    ###############################################################################
    # select and delete users who don't replied the email after 31 days

    deletion_accepted = False

    query = "select account_id, username, email, to_be_deleted, feedback, recipient_error, elapsed_days from " + mailing_db_table + " where not feedback and elapsed_days = '31'"

    delete_inactives(deletion_accepted, query)
